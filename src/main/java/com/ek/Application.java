package com.ek;

import com.ek.controller.EncryptionProvider;
import com.ek.utils.Parser;
import com.ek.utils.PreProcessor;
import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.ek.utils.Common.getHiddenData;
import static com.ek.utils.Common.readObject;

public class Application {

    public static boolean debugMode = false;
    private final Parser parser = new Parser();

    private final EncryptionProvider encryptionProvider = new EncryptionProvider();

    @SneakyThrows
    public static void main(String[] args) {
        new Application().launch(args);
    }

    @SneakyThrows
    private void launch(String[] args) {
        List<String> parseToList = parser.parse(argsParser(args));
        PreProcessor.begin(parseToList);
    }

    @SneakyThrows
    public String argsParser(String[] args) {
        if (args.length == 0) {
            return "";
        }
        if (!args[0].equals("-f") && !args[0].equals("-k") && !args[0].equals("-p")) {
            return String.join(" ", args);
        }
        byte[] result = null;
        Object decryptionKey = null;
        boolean password = true;
        Map<String, String> replacers = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            if (Objects.equals(args[i], "-f")) {
                result = Files.readAllBytes(Paths.get(args[++i]));
            } else if (args[i].equals("-k")) {
                decryptionKey = readObject(args[++i]);
                password = false;
            } else if (args[i].equals("-p")) {
                if (i + 1 < args.length && !args[i + 1].startsWith("-")) {
                    decryptionKey = args[++i].toCharArray();
                } else {
                    decryptionKey = getHiddenData("Enter decryption password: ");
                }
            } else if (args[i].contains(":")) {
                var keyVal = args[i].split(":");
                replacers.put(keyVal[0], keyVal[1]);
            } else if (args[i].equals("-d")) {
                debugMode = true;
            }
        }

        String script = encryptionProvider.decrypt(decryptionKey, result, password);

        for (var replacer : replacers.entrySet()) {
            script = script.replace(replacer.getKey(), replacer.getValue());
        }

        return script;
    }
}
