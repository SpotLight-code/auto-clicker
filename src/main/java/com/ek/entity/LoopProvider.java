package com.ek.entity;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class LoopProvider {

    @Getter
    private final List<String> scriptAfterLoopProcess = new ArrayList<>();

    private final List<String> loopBuilder = new ArrayList<>();
    private boolean loopInitialized = false;
    int repeatTimes = 0;

    public void append(String str) {
        if (str.startsWith("loop:")) {
            loopInitialized = true;
            repeatTimes = Integer.parseInt(str.substring(str.indexOf(":") + 1).trim());
        } else if (str.equals("endLoop")) {
            IntStream.range(0, repeatTimes)
                    .mapToObj(i -> loopBuilder)
                    .forEachOrdered(scriptAfterLoopProcess::addAll);
            loopBuilder.clear();
            loopInitialized = false;
        } else {
            if (loopInitialized) {
                loopBuilder.add(str);
            } else {
                scriptAfterLoopProcess.add(str);
            }
        }
    }


}
