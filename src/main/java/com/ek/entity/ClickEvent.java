package com.ek.entity;

import com.ek.utils.Clicker;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class ClickEvent {
    private static final Map<String, Integer> map = new HashMap<>();

    static {
        map.put("tab", KeyEvent.VK_TAB);
        map.put("shift", KeyEvent.VK_SHIFT);
        map.put("space", KeyEvent.VK_SPACE);
        map.put("%", KeyEvent.VK_5);
        map.put("enter", KeyEvent.VK_ENTER);
        map.put("down", KeyEvent.VK_DOWN);
        map.put("ctrl", KeyEvent.VK_CONTROL);
        map.put("alt", KeyEvent.VK_ALT);
        map.put("left", MouseEvent.BUTTON1_DOWN_MASK);
        map.put("right", MouseEvent.BUTTON3_DOWN_MASK);
        map.put("leftBtn", KeyEvent.VK_LEFT);
        map.put("rightBtn", KeyEvent.VK_RIGHT);
        map.put("upBtn", KeyEvent.VK_UP);
        map.put("caps", KeyEvent.VK_CAPS_LOCK);

    }

    private final EventType eventType;
    private Integer scanCode;
    private int waitTime = 10;

    private Integer x;
    private Integer y;

    public ClickEvent(String clickEvent) {
        if (clickEvent.startsWith("hold:")) {
            this.eventType = EventType.buttonClick;
            this.scanCode = getScanCode(clickEvent, clickEvent.indexOf(',') + 1);
            this.waitTime = getWaitTime(clickEvent);
        } else if (!clickEvent.contains(":")) {
            this.eventType = EventType.buttonClick;
            this.scanCode = getScanCode(clickEvent, 0);
        } else {
            var typeWithData = clickEvent.split(":");
            eventType = EventType.valueOf(typeWithData[0].trim());

            if (eventType.equals(EventType.mouseClick)) {
                this.scanCode = map.get(typeWithData[1].trim());
            } else {
                var coordinates = typeWithData[1].split(",");
                this.x = parseInt(coordinates[0].trim());
                this.y = parseInt(coordinates[1].trim());
            }
        }
    }

    private int getWaitTime(String clickEvent) {
        return Integer.parseInt(clickEvent.substring(clickEvent.indexOf(':') + 1, clickEvent.indexOf(',')).trim());
    }

    private Integer getScanCode(String clickEvent, int i) {
        return map.getOrDefault(clickEvent,
                KeyEvent.getExtendedKeyCodeForChar(clickEvent.charAt(i)));
    }

    public void press(Robot robot) {
        switch (eventType) {
            case mouseSeek -> robot.mouseMove(x, y);
            case buttonClick -> robot.keyPress(scanCode);
            case mouseClick -> robot.mousePress(scanCode);
        }
        Clicker.waitMs(waitTime);
    }

    public void release(Robot robot) {
        switch (eventType) {
            case buttonClick -> robot.keyRelease(scanCode);
            case mouseClick -> robot.mouseRelease(scanCode);
        }
        Clicker.waitMs(10);
    }
}
