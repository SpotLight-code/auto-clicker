package com.ek.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class ScriptWithDelayBeforeStart {
    private final List<String> script = new ArrayList<>();
    private long waitTime = 0;

    public void addToScript(String str) {
        script.add(str);
    }

    public void addToScript(List<String> str) {
        script.addAll(str);
    }
}
