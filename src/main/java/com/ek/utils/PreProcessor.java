package com.ek.utils;

import com.ek.entity.LoopProvider;
import com.ek.entity.ScriptWithDelayBeforeStart;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PreProcessor {

    public static void begin(List<String> list) {
        list = parseLoop(list);
        list = parseRand(list);
        extractDelays(list).forEach(PreProcessor::launchScriptWithDelay);
    }

    private static List<String> parseRand(List<String> list) {
        return list.stream()
                .flatMap(PreProcessor::parseRand)
                .collect(Collectors.toList());
    }

    public static List<String> parseLoop(List<String> list) {
        var loopProvider = new LoopProvider();
        list.forEach(loopProvider::append);
        return loopProvider.getScriptAfterLoopProcess();
    }

    private static void launchScriptWithDelay(ScriptWithDelayBeforeStart e) {
        Clicker.clicker(e.getScript());
        Clicker.waitMs(e.getWaitTime());
    }

    private static List<ScriptWithDelayBeforeStart> extractDelays(List<String> list) {
        int counter = 0;
        List<ScriptWithDelayBeforeStart> listWithDelay = new ArrayList<>();
        listWithDelay.add(new ScriptWithDelayBeforeStart());

        for (String str : list) {
            if (str.startsWith("wait:")) {
                listWithDelay.get(counter++).setWaitTime(Long.parseLong(str.substring(5).trim()));
                listWithDelay.add(new ScriptWithDelayBeforeStart());
                continue;
            }
            listWithDelay.get(counter).addToScript(str);
        }
        return listWithDelay;
    }

    private static Stream<? extends String> parseRand(String s) {
        return s.equals("uuid") ? String.valueOf(UUID.randomUUID()).substring(25).chars().mapToObj(String::valueOf) : Stream.of(s);
    }
}
