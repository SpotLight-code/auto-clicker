package com.ek.utils;

import java.io.Console;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;

public class Common {

    public static Object readObject(String path) {
        try (var file = new FileInputStream(path); var in = new ObjectInputStream(file)) {
            return in.readObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readObject(String path, Class<T> tClass) {
        return tClass.cast(readObject(path));
    }

    public static void writeObject(String path, Object object) {
        try (var file = new FileOutputStream(path); var out = new ObjectOutputStream(file)) {
            out.writeObject(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Object getHiddenData(String msg) {
        return Optional.ofNullable(System.console())
                .map(console -> readConsole(msg, console))
                .orElseThrow(() -> new RuntimeException("Unable to get console, you can't you param '-p' without specifying password from this way of starting program"));
    }

    private static char[] readConsole(String msg, Console console) {
        System.out.println(msg);
        return console.readPassword();
    }
}
